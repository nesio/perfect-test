<?php

use Illuminate\Database\Seeder;
use App\StatusVenda;

class StatusVendaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * 
         */
        StatusVenda::create([
                'nome' => 'Vendidos'
            ]);
        
        $this->command->info('Status criado com sucesso');
        
        /*
         * 
         */
        StatusVenda::create([
                'nome' => 'Devolução'
            ]);
        
        $this->command->info('Status criado com sucesso');
        
        /*
         * 
         */
        StatusVenda::create([
                'nome' => 'Cancelados'
            ]);
        
        $this->command->info('Status criado com sucesso');
    }
}
