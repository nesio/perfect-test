@extends('layout')

@section('content')
    <h1>Dashboard de vendas</h1>
    <div class='card mt-3'>
        <div class='card-body'>
            <h5 class="card-title mb-5">Tabela de vendas
                <a href='{{ url("/sales/create") }}' class='btn btn-secondary float-right btn-sm rounded-pill'><i class='fa fa-plus'></i>  Nova venda</a></h5>
            <form>
                <div class="form-row align-items-center">
                    <div class="col-sm-5 my-1">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Clientes</div>
                            </div>
                            <select class="form-control" id="inlineFormInputName">
                                @foreach($clientes as $cliente)
                                    <option value="{{ $cliente->id }}">{{ $cliente->nome }}</option> 
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6 my-1">
                        <label class="sr-only" for="inlineFormInputGroupUsername">Username</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Período</div>
                            </div>
                            <input type="text" class="form-control date_range" id="inlineFormInputGroupUsername" placeholder="Username">
                        </div>
                    </div>
                    <div class="col-sm-1 my-1">
                        <button type="submit" class="btn btn-primary" style='padding: 14.5px 16px;'>
                            <i class='fa fa-search'></i></button>
                    </div>
                </div>
            </form>
            <table class='table'>
                <tr>
                    <th scope="col">
                        Produto
                    </th>
                    <th scope="col">
                        Data
                    </th>
                    <th scope="col">
                        Valor
                    </th>
                    <th colspan="2" scope="col">
                        Ações
                    </th>
                </tr>
                @foreach($vendas as $venda)
                    <tr>
                        <td>
                            {{ $venda->nome }}
                        </td>
                        <td>
                            {{ $venda->data }}
                        </td>
                        <td>
                           R$ {{ number_format($venda->valor,2,',','.') }}
                        </td>
                        <td>
                            <a href='{{ url("/sales/{$venda->id}/edit") }}' class='btn btn-primary'>Editar</a>
                        </td> 
                        <td>
                            <form action="sales/{{ $venda->id }} " method="post">
                                
                                @csrf  
                                @method('delete')
                                <input type="submit" class='btn btn-danger'  value="Deletar" />
                            </form>
                        </td> 
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
    <div class='card mt-3'>
        <div class='card-body'>
            <h5 class="card-title mb-5">Resultado de vendas</h5>
            <table class='table'>
                <tr>
                    <th scope="col">
                        Status
                    </th>
                    <th scope="col">
                        Quantidade
                    </th>
                    <th scope="col">
                        Valor Total
                    </th>
                </tr>
                @foreach($totalizador as $total)
                <tr>
                    <td>
                        {{ $total->nome }}
                    </td>
                    <td>
                        {{ $total->qnt }}
                    </td>
                    <td>
                       R$ {{ number_format($total->prc,2,',','.') }}
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>

    <div class='card mt-3'>
        <div class='card-body'>
            <h5 class="card-title mb-5">Produtos
                <a href='{{ url("/products/create") }}' class='btn btn-secondary float-right btn-sm rounded-pill'><i class='fa fa-plus'></i>  Novo produto</a></h5>
            <table class='table'>
                <tr>
                    <th scope="col">
                        Nome
                    </th>
                    <th scope="col">
                        Valor
                    </th>
                    <th scope="col">
                        Ações
                    </th>
                </tr>
                @foreach($produtos as $produto)
                <tr>
                    <td>
                        {{ $produto->nome }}
                    </td>
                    <td>
                       R$ {{ number_format($produto->preco,2,',','.') }}
                    </td>
                    <td>
                        <a href='{{ url("/products/{$produto->id}/edit") }}' class='btn btn-primary'>Editar</a>
                    </td>
                </tr> 
                @endforeach
            </table>
        </div>
    </div>
@endsection
