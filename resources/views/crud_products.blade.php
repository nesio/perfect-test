@extends('layout')

@section('content')
    <h1>{{ isset($produto['id'])?'Editar Produto':'Adicionar' }}</h1>
    <div class='card'>
        <div class='card-body'>
            <form action='{{ (!empty($produto['id'])) ? "/products/{$produto['id']}" : route('products.store') }}' method="post">
                
                @csrf
                
                @if(!empty($produto))
                    @method('PUT')
                @endif
                
                <div class="form-group">
                    <label for="nome">Nome do produto</label>
                    <input type="text" class="form-control "  value="{{ !empty($produto['nome'])?$produto['nome']:"" }}" name="nome">
                </div>
                <div class="form-group">
                    <label for="descricao">Descrição</label>
                    <textarea type="text" rows='5' class="form-control" name="descricao">{{ !empty($produto['descricao'])?$produto['descricao']:"" }}</textarea>
                </div>
                <div class="form-group">
                    <label for="preco">Preço</label>
                    <input type="text" class="form-control" name="preco"  value="{{ !empty($produto['preco'])?$produto['preco']:"" }}" placeholder="100,00 ou maior">
                </div>
                <button type="submit" class="btn btn-primary">Salvar</button>
            </form>
        </div>
    </div>
@endsection
