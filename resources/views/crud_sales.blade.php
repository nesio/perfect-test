@extends('layout')

@section('content')
    <h1> {{ isset($venda['id'])?'Editar Venda':'Adicionar' }}</h1>
    <div class='card'>
        <div class='card-body'>
            <form action="{{ (!empty($venda['id'])) ? "/sales/{$venda['id']}" : route('sales.store') }}" method="POST">
                @csrf
                
                @if(!empty($venda))
                    @method('PUT')
                @endif
                        
                <h5>Informações do cliente</h5>
                <div class="form-group">
                    <label for="cliente">Cliente</label>
                    <select name="cliente" class="form-control">
                        <option selected>Escolha...</option>
                        @foreach($clientes as $cliente)
                                
                            <option value="{{ $cliente->id }}" {{ (!empty($venda) && $cliente->id == $venda['cliente_id'])?"selected":"" }}>{{ $cliente->nome }}</option>
                        @endforeach
                    </select>
                </div>
                <h5 class='mt-5'>Informações da venda</h5>
                <div class="form-group">
                    <label for="product">Produto</label>
                    <select name="product" class="form-control">
                        <option selected>Escolha...</option>
                        @foreach($produtos as $produto)
                            <option value="{{ $produto->id }}"  {{ (!empty($venda) && $produto->id == $venda['produto_id'])?"selected":"" }}>{{ $produto->nome }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="quantity">Quantidade</label>
                    <input type="text" class="form-control" name="quantidade" value="{{ !empty($venda['quantidade'])?$venda['quantidade']:"" }}" placeholder="1 a 10">
                </div>
                <div class="form-group">
                    <label for="discount">Desconto</label>
                    <input type="text" class="form-control" name="desconto" value="{{ !empty($venda['desconto'])?$venda['desconto']:"" }}" placeholder="100,00 ou menor">
                </div>
                <div class="form-group">
                    <label for="status">Status</label>
                    <select name="status" class="form-control">
                        <option {{ (!empty($venda))?"":"selected" }}>Escolha...</option>
                        <option value="1" {{ (!empty($venda) && $venda['status']== 1)?"selected":"" }}>Vendidos</option> 
                        <option value="2" {{ (!empty($venda) && $venda['status']== 2)?"selected":""  }}>Devolvido</option>
                        <option value="3" {{ (!empty($venda) && $venda['status']== 3)?"selected":""  }}>Cancelados</option>
                    </select>
                </div>
                <input type="hidden" name="id" value="{{ !empty($venda['id'])?$venda['id']:"" }}" />
                <button type="submit" class="btn btn-primary">{{ !empty($venda['id'])?"Editar":"Salvar" }}</button>
            </form>
        </div>
    </div>
@endsection
