<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Venda extends Model
{
    protected $table = 'vendas';
    protected $primaryKey = 'id';

    protected $fillable = [
        "produto_id",
        "cliente_id", 
        "quantidade",
        "desconto",
        "status"
    ];
    
    protected $guarded  = [
        'id', 'created_at', 'updated_at' 
    ];
    
    /*
     * 
     */
    static public function listaVendas(){
        $qry = DB::select("SELECT A.id, B.nome, ((B.preco * A.quantidade) - A.desconto) AS valor, DATE_FORMAT(A.created_at,'%d/%m/%Y %Hh%i') AS data
                            FROM vendas AS A  
                            LEFT JOIN produtos AS B ON(A.produto_id = B.id)");
        return $qry;
    }
    
    /*
     * 
     */
    static public function totalizadorVendas(){
        $qry = DB::select("SELECT nome, SUM(qnt) AS qnt, SUM(prc) AS prc FROM (  
                                    SELECT B.nome, (A.quantidade) AS qnt, ((A.quantidade * C.preco) - A.desconto) AS prc
                                FROM vendas AS A  
                              LEFT JOIN status_vendas AS B ON(A.status = B.id)
                              LEFT JOIN produtos AS C ON(A.produto_id = C.id)
                            ) AS tmp GROUP BY nome");
        return $qry;
        
    }
}
