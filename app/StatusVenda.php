<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusVenda extends Model
{
    protected $table = 'status_vendas';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        "nome"
    ];
    
    protected $guarded  = [
        'id'
    ];
}
