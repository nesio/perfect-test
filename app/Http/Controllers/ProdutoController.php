<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produto;

class ProdutoController extends Controller
{
    
    public function create(Request $request){
        
         
        return view('crud_products');
        
    }
    
    /*
     * 
     */
    public function store(Request $request)
    {
         
        $produto = [
            "nome" => $request->nome,
            "descricao" => $request->descricao,
            "preco" => $request->preco 
        ];
        
        Produto::create($produto);
        
        return redirect('/');
     
    }
    
    /**
     * 
     * @param Request $request
     */
    public function edit(Request $request, $id){
        
        
        $produto = Produto::where("id","=",$id)->get()[0];
         
        return view('crud_products',["produto" => $produto]);
        
    }
    
    /*
     * 
     */
    public function update(Request $request, $id){
        
        $produto = Produto::findOrFail($id); 
        $produto->nome    = $request->nome;
        $produto->descricao    = $request->descricao; 
        $produto->preco    = $request->preco;  
         
        $produto->save();
        
        return redirect('/');
        
    }
}
