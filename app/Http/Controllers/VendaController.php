<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Venda;
use App\Cliente;
use App\Produto;

class VendaController extends Controller
{
    
    public function create(Request $request){
        
        $produtos = Produto::all();
        $clientes = Cliente::all();
        
        return view('crud_sales',["produtos" => $produtos, "clientes" => $clientes]);
        
    }
    
    /*
     * 
     */
    public function store(Request $request)
    {
         
        $venda = [
            "produto_id" => $request->product,
            "cliente_id" => $request->cliente,
            "quantidade" => $request->quantidade,
            "desconto" => number_format($request->desconto,2,'.',''),
            "status" => $request->status
        ];
        
        Venda::create($venda);
        
        return redirect('/');
     
    }
    
    /**
     * 
     * @param Request $request
     */
    public function edit(Request $request, $id){
        
        $produtos = Produto::all();
        $clientes = Cliente::all();
        $venda = Venda::where("id","=",$id)->get()[0];
         
        return view('crud_sales',["produtos" => $produtos, "clientes" => $clientes, "venda" => $venda]);
        
    }
    
    /*
     * 
     */
    public function update(Request $request, $id){
        
        $venda = Venda::findOrFail($id); 
        $venda->produto_id    = $request->product;
        $venda->cliente_id    = $request->cliente; 
        $venda->quantidade    = $request->quantidade; 
        $venda->desconto      = $request->desconto; 
        $venda->status        = $request->status; 
         
        $venda->save();
        
        return redirect('/');
        
    }
    
    /*
     * 
     */
    public function destroy(Request $request, $id){
         
        $venda = Venda::findOrFail($id);
        $venda->delete();
        return redirect('/');
        
    }
}
