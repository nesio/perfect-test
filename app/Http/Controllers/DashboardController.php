<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;
use App\Venda;
use App\Produto;

class DashboardController extends Controller {

    public function index(Request $request) {
        /**
         * LISTA DE CLIENTES
         */
        $clientes = Cliente::all();

        /*
         * 
         */
        $vendas = Venda::listaVendas();
        
        /*
         * 
         */
        $totalizador = Venda::totalizadorVendas();
        
        /*
         * 
         */
        $produtos = Produto::all();
         
        return view("dashboard", ["clientes" => $clientes, "vendas" => $vendas, "totalizador" => $totalizador, "produtos" => $produtos]);
        
    }

}
