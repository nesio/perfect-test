<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    protected $table = 'produtos';
    protected $primaryKey = 'id';

    protected $fillable = [
        "nome",
        "descricao", 
        "preco", 
    ];
    
    protected $guarded  = [
        'id', 'created_at', 'updated_at' 
    ];
}
