<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = 'clientes';
    protected $primaryKey = 'id';

    protected $fillable = [
        "nome",
        "email",
        "cpf" 
    ];
    
    protected $guarded  = [
        'id', 'created_at', 'updated_at' 
    ];
}
